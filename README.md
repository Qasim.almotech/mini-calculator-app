# Add3 Calculator

## Description

Using Add3, you deploy four no-code token contracts to make fundraising possible. First, you deploy a Minting Contract and mint tokens that can fund your project. You schedule token distribution via a Vesting & Distribution Contract. To promote participation and utility, you deploy a Staking Contract where users can earn rewards. To provide further utility, you deploy an LP Staking Contract to allow users to earn LP tokens.

## Installation

- Clone the repo in your terminal by clicking the _green_ clone or download button at the top right and copyin the url
- In your terminal, type `git clone URL`
  - replace URL with the url you copied
  - hit enter
- This will copy all the files from this repo down to your computer
- In your terminal, cd into the directory you just created
- Type `npm install` to install all dependencies
- Last, but not least, type `npm start` to run the app locally.

- To look at the code, just open up the project in your favorite code editor!
