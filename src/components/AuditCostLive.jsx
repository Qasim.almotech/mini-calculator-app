import React from 'react';

const AuditCostLive = ({ selectedContract }) => {
  // Use the selectedValues data in this component as needed
  let totalContract = (Object.keys(selectedContract).length > 0) ? Object.keys(selectedContract).length : 0;
  
  if(selectedContract.find(item => item.key === "Liquidity Pool Provision (LP Token)")){
    totalContract -=1
  }
  return (
    <div>
        {'$' + (totalContract * 15000).toLocaleString()}
    </div>
  );
};

export default AuditCostLive;
