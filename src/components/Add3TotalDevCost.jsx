import React from 'react';

const Add3TotalDevCost = ({ selectedContract }) => {
  // Use the selectedValues data in this component as needed
  let contracts = Object.keys(selectedContract).length;
  
  var result = selectedContract.find(item => item.key === "Liquidity Pool Provision (LP Token)")

  
  let sumNativeFee = selectedContract.reduce(function(prev, current) {
    return prev + +current.nativeFee
  }, 0);
  
// after calculate AVG we can -1 contracts
  if(result){
    contracts -=1
  }

  let activeCost = 0
  if(contracts > 0 ){
    activeCost = (contracts * 3000)
  }
      
  return (
    <div>
      ${Math.round((activeCost  + sumNativeFee)).toLocaleString()}    
    </div>
  );
};

export default Add3TotalDevCost;
