import React, { Component } from 'react';
import { Multiselect } from "multiselect-react-dropdown";


class ContractSelectOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [
        { key: 'Token Minting', value: '2', nativeFee:'100' },
        { key: 'Liquidity Pool Provision (LP Token)', value: '2', nativeFee:'0' },
        { key: 'Vesting', value: '45', nativeFee:'100' },
        { key: 'Fixed Staking', value: '30', nativeFee:'100' },
        { key: 'Dynamic Staking', value: '30', nativeFee:'100' },
        { key: 'Liquidity Pool Staking', value: '30', nativeFee:'100' },
        
        // Add more options as needed
      ],
      selectedContract: [],
      selectedValues: [],
      
    };
    this.style = {
        chips: {
          background: "red"
        },
        searchBox: {
          border: "none",
          "border-bottom": "1px solid blue",
          "border-radius": "0px"
        }
      };
  }
  
  componentDidMount() {

    this.handleSelect(this.state.selectedValues);
    
  }
  handleSelect = (selectedList) => {
    
    this.props.selectedContract(selectedList);
    
  };

  

  render() {
    const { options, selectedValues} = this.state;
    return (
      <div>
            <Multiselect
              options={options}
              displayValue="key"
              onSelect={this.handleSelect}
              onRemove={this.handleSelect}
              selectedValues={selectedValues}
              showCheckbox="true"
              placeholder="Select Smart Contracts You Will build"
              avoidHighlightFirstOption="true"
              showArrow="true"
            />
      </div>
    );
  }
}

export default ContractSelectOption;
