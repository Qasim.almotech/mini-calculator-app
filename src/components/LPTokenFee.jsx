import React from 'react';

const LPTokenFee = ({ selectedContract }) => {
  // Use the selectedValues data in this component as needed
  // let contracts = Object.keys(selectedContract).length;
  var result = selectedContract.find(item => item.key === "Liquidity Pool Provision (LP Token)"); 
  let nativeFee = selectedContract.reduce(function(prev, current) {
    return prev + +current.nativeFee
  }, 0);



  
    return (
      <>
        ${nativeFee}
      </>
    );
  
};

export default LPTokenFee;
