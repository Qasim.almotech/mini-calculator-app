import React from 'react';

const ContractSelected = ({ selectedContract }) => {
  
  let length = 0;
    if (selectedContract) {
      length = Object.keys(selectedContract).length;
    }
  return (
    <div>
      { length }
    </div>
  );
};

export default ContractSelected;