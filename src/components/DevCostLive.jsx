import React from 'react';

const DevCostLive = ({ selectedContract, serviceSelectOption }) => {
  // Use the selectedValues data in this component as needed  
 
  let sumSelectedValues = selectedContract.reduce(function(prev, current) {
    return prev + +current.value
  }, 0);
  let sumSevicesSelectedValues = 0
  if(serviceSelectOption){ 
      sumSevicesSelectedValues = serviceSelectOption.reduce(function(prev, current) {
      return prev + +current.value
    }, 0);
  }
  let sum = sumSelectedValues + sumSevicesSelectedValues

  return (
    <div>
      {'$'+ Math.round((sum * 230.769)).toLocaleString() +' - $'+ Math.round((sum * 538.462)).toLocaleString()}
    </div>
  );
};

export default DevCostLive;
