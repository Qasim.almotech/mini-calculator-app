import React from 'react';

const DaysLiveCurrentDevTop = ({ selectedContract, serviceSelectOption, developerValue }) => {

 
  let sumSelectedValues = selectedContract.reduce(function(prev, current) {
    return prev + +current.value
  }, 0);
  
  let sumSevicesSelectedValues = 0
  if(serviceSelectOption){ 
      sumSevicesSelectedValues = serviceSelectOption.reduce(function(prev, current) {
      return prev + +current.value
    }, 0);
  }

  let contracts = Object.keys(selectedContract).length;
  let delayDays = 0
  if(contracts > 0 ){
    var result = selectedContract.find(item => item.key === "Token Minting");
    if(contracts === 6){
        delayDays = 45
    }else{

        if(result){
        contracts = contracts - 1
        delayDays += 5
        }
        
        if(contracts > 0){
            delayDays += (contracts * 10)
        }
    }
      
  }

  let divid = (developerValue > 1) ?  developerValue : 1;
  // let delayDay = (Object.keys(selectedContract).length > 0) ?  15 : 0;
  let sum = (sumSelectedValues + sumSevicesSelectedValues) / divid

  return (
    <div>
      {Math.round( sum + delayDays ) + ((developerValue > 1 && Object.keys(selectedContract).length > 0) ?  1 : 0) } Days
    </div>
  );
};

export default DaysLiveCurrentDevTop;
