import React from 'react';

const DelayDays = ({ selectedContract,  developerValue }) => {
  // Use the selectedValues data in this component as needed
  let contracts = Object.keys(selectedContract).length;
  let delayDays = 0
  if(contracts > 0 ){
    var result = selectedContract.find(item => item.key === "Token Minting");
    if(contracts === 6){
        delayDays = 45
    }else{

        if(result){
        contracts = contracts - 1
        delayDays += 5
        }
        
        if(contracts > 0){
            delayDays += (contracts * 10)
        }
    }
      
  }

  return (
    <div>
        { delayDays } Days
    </div>
    
  );
  
};

export default DelayDays;
