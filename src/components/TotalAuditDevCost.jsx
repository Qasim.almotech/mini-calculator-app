import React from 'react';

const TotalAuditDevCost = ({ selectedContract, serviceSelectOption }) => {
  // Use the selectedValues data in this component as needed

  let sum = 0;
  let totalContract = 0;
  let sumSelectedValues = 0
  if(selectedContract){
    sumSelectedValues = selectedContract.reduce(function(prev, current) {
      
      return prev + +current.value

    }, 0);
  }
    
  let sumSevicesSelectedValues = 0
  if(serviceSelectOption){
    sumSevicesSelectedValues = serviceSelectOption.reduce(function(prev, current) {
      
      return prev + +current.value
    }, 0);
  }

  sum = sumSelectedValues + sumSevicesSelectedValues
  
  totalContract = (Object.keys(selectedContract).length > 0) ? Object.keys(selectedContract).length : 0 
  if(selectedContract.find(item => item.key === "Liquidity Pool Provision (LP Token)")){
    totalContract -=1
  }
  
  return (
    <div>
      {'$'+Math.round((sum * 538.462) + (totalContract * 15000)).toLocaleString()}
    </div>
  );
};

export default TotalAuditDevCost;
