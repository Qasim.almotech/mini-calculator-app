import Form from 'react-bootstrap/Form';
import React, { useState } from 'react';

const Developer = ({ onIntegerChange }) => {
  const [integerValue, setIntegerValue] = useState(1);

  const handleChange = (event) => {
    const input = event.target.value;
    setIntegerValue(input);
    onIntegerChange(parseInt(input, 10)); // Parse input to an integer and pass it to the parent component
  };

  return (
    <div>
        <Form.Control 
            type="number"
            value={integerValue}
            onChange={handleChange}
            min="1"
            className='add3-input'
        />
    </div>
  );
};

export default Developer;
