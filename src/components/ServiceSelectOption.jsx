import React, { Component } from 'react';
import { Multiselect } from "multiselect-react-dropdown";


class ServiceSelectOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [
        { key: 'dApp Terminal', value: '60' },
        { key: 'Analytics Dashboard', value: '60' },
        
        // Add more options as needed
      ],
      serviceSelectOption: [],
      selectedValues: [],
      
    };
    this.style = {
        chips: {
          background: "red"
        },
        searchBox: {
          border: "none",
          "border-bottom": "1px solid blue",
          "border-radius": "0px"
        }
      };
  }
  
  componentDidMount() {
    
    this.handleSelect(this.state.selectedValues);
    
  }
  handleSelect = (selectedList) => {
    
    this.props.serviceSelectOption(selectedList);
    
  };

  

  render() {
    const { options, selectedValues} = this.state;
    return (
      <div>
            <Multiselect
              options={options}
              displayValue="key"
              onSelect={this.handleSelect}
              onRemove={this.handleSelect}
              selectedValues={selectedValues}
              showCheckbox="true"
              placeholder="Select Services You Will Build"
              closeIcon="circle"
              keepSearchTerm="false"
              avoidHighlightFirstOption="true"
              showArrow="true"
            />
      </div>
    );
  }
}

export default ServiceSelectOption;
