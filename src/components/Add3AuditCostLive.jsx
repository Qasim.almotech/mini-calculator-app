import React from 'react';

const Add3AuditCostLive = ({ selectedContract }) => {
  // Use the selectedValues data in this component as needed
  let totalContract = (Object.keys(selectedContract).length > 0) ? Object.keys(selectedContract).length : 0;
  
  if(selectedContract.find(item => item.key === "Liquidity Pool Provision (LP Token)")){
    totalContract -=1
  }
  return (
    <div>
        {'$' + (totalContract * 3000).toLocaleString()}
    </div>
  );
};

export default Add3AuditCostLive;
