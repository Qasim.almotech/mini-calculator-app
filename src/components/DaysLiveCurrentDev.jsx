import React from 'react';

const DaysLiveCurrentDev = ({ selectedContract, serviceSelectOption, developerValue }) => {
  // Use the selectedValues data in this component as needed
 
  let sumSelectedValues = selectedContract.reduce(function(prev, current) {
    return prev + +current.value
  }, 0);
  let sumSevicesSelectedValues = 0
  if(serviceSelectOption){ 
      sumSevicesSelectedValues = serviceSelectOption.reduce(function(prev, current) {
      return prev + +current.value
    }, 0);
  }

  let divid = (developerValue > 1) ?  developerValue : 1;

  let sum = (sumSelectedValues + sumSevicesSelectedValues) / divid

  return (
    <div>
      {Math.round( sum ) + ((developerValue > 1) ?  1 : 0) }
    </div>
  );
};

export default DaysLiveCurrentDev;
