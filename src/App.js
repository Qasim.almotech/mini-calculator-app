import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ContractSelected from './components/ContractSelected';
import DaysLiveOneReadOnly from "./components/DaysLiveOneReadOnly";
import Developer from './components/Developer';
import DaysLiveCurrentDev from './components/DaysLiveCurrentDev'
import DevCostLive from './components/DevCostLive';
import AuditCostLive from './components/AuditCostLive';
import TotalAuditDevCost from './components/TotalAuditDevCost';
import Add3TotalDevCost from './components/Add3TotalDevCost';
import DaysLiveCurrentDevTop from './components/DaysLiveCurrentDevTop';
import ContractSelectOption from './components/ContractSelectOption';
import ServiceSelectOption from './components/ServiceSelectOption';
import DelayDays from './components/DelayDays';
import Add3AuditCostLive from './components/Add3AuditCostLive';
import LPTokenFee from './components/LPTokenFee';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      developerValue: [],
      multiSelectedValues: [],
      selectedContract: [],
      serviceSelectOption: [],
    };
    
  }
  handleServicesSelectedValuesChange = (serviceSelectOption) => {

    this.setState({ serviceSelectOption });
    // console.log('app:' + serviceSelectOption)
  };
  
  handleIntegerChange = (developerValue) => {
    this.setState({ developerValue });
  };
  handleContractData = (selectedContract) => {
    this.setState({ selectedContract });
  };

  
  
  render() {
    const { developerValue } = this.state;
    const { selectedContract } = this.state;
    const { serviceSelectOption } = this.state;
    let devCostBeforeGolive = '$0'
    let workingDelayDayAdd3 = 0
    
    if(Object.keys(selectedContract).length > 0){
      devCostBeforeGolive = '$0'
      workingDelayDayAdd3 = 4
      
    }
    
    return (
      <div className="App">
        
      <Container className='withe-border-shadow'>
        
            <Row className="header justify-content-md-center">
              <Col md={3} sm="auto" xs="6">
                
              <table className="table table-bordered vs-table add3-table right">
                <tbody>
                  <tr>
                    <th>
                    Cost & days to go live
                    </th>
                  </tr>
                  <tr>
                    <td>
                      <TotalAuditDevCost selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                      <small className="text-muted"> 
                      Depending On Salary
                      </small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <DaysLiveCurrentDevTop selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} developerValue={developerValue} />
                    </td>
                  </tr>
                </tbody>
              </table>
              </Col>
              <Col md="auto" sm="auto" xs="6">
                <p className='top-50'>&#8592; In-house Cost</p>
              </Col>
              <Col md="auto" sm="auto" xs="6">
                <p className='right-text top-50'>Add3 Comparison &#8594;</p>
              </Col>
              <Col md={3} lg={3} sm="auto" xs="6">
              <table className="table table-bordered vs-table add3-table">
                <tbody>
                  <tr>
                    <th>
                    Cost & days to go live
                    </th>
                  </tr>
                  <tr>
                    <td className='percentage-total'>
                      <Add3TotalDevCost selectedContract={selectedContract} />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      { workingDelayDayAdd3 } Days
                    </td>
                  </tr>
                </tbody>
              </table>
              </Col>
            </Row>
            <Row className='top-bar justify-content-md-center'>
              <Col xs md="9" lg={6}>
                <h5 className='text-center'>Select Contracts & Services</h5>
                <ContractSelectOption selectedContract={this.handleContractData}/>
              </Col>
            </Row>
            <Row className='justify-content-md-center'>
                <Col xs md="9" lg={6}>
                  <ServiceSelectOption serviceSelectOption={this.handleServicesSelectedValuesChange}/>
                </Col>
            </Row>
            {/* the table for mobile size  */}
            <Row className='mobile main-content justify-content-md-center'>
              
              <Col md={6} lg={5}>
              <table className="table table-bordered right">
                <thead>
                  <tr>
                    <th scope="col" colSpan={2}>
                      Time and cost to develop on your own
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Number of Full Time Developers
                    </td>
                    <th className='no-pad'> 
                      <Developer onIntegerChange={this.handleIntegerChange} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Contracts Selected
                    </td>
                    <th> 
                      <ContractSelected selectedContract={selectedContract}/>
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Days to go live with 1 Dev
                    </td>
                    <th> 
                      <DaysLiveOneReadOnly selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Days to go live with current Dev
                    </td>
                    <th> 
                      <DaysLiveCurrentDev selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} developerValue={developerValue} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Dev Cost Before Go-live<span className='sub-text'>2</span>
                    </td>
                    <th> 
                      <DevCostLive selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Audit Cost Before Go-live
                    </td>
                    <th> 
                      <AuditCostLive selectedContract={selectedContract} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Working Day Delay by Audit
                    </td>
                    <th> 
                      <DelayDays selectedContract={selectedContract} developerValue={developerValue} />
                    </th>
                  </tr>
                  <tr>
                    <th>
                      Total Audit and Dev Costs
                    </th>
                    <th> 
                      <TotalAuditDevCost selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                    </th>
                  </tr>
                  
                </tbody>
              </table>
                
              </Col>
              <Col md={6} lg={5}>
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col" colSpan={2}>
                      Time and cost using Add3
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Developers needed
                    </td>
                    <th> 
                        0
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Contracts Selected
                    </td>
                    <th> 
                      <ContractSelected selectedContract={selectedContract}/>
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Days to go live with 0 Dev
                    </td>
                    <th> 
                      1 
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Native Gas Token Fee
                    </td>
                    <th>
                      <LPTokenFee selectedContract={selectedContract}/>
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Dev Cost Before Go-live
                    </td>
                    <th> 
                      { devCostBeforeGolive }
                    </th>
                  </tr>
                  <tr>
                    <td>
                     Audit Cost Before Go-Live<span className='sub-text'>1</span>
                    </td>
                    <th> 
                      <Add3AuditCostLive selectedContract={selectedContract} />
                    </th>
                  </tr>
                  <tr>
                    <td>
                      Working Day Delay by Audit
                    </td>
                    <th> 
                      { workingDelayDayAdd3 } Days
                    </th>
                  </tr>
                  <tr>
                    <th>
                      Token fee + Audit Cost
                    </th>
                    <th className='percentage-total'> 
                      <Add3TotalDevCost selectedContract={selectedContract} />
                    </th>
                  </tr>
                  
                </tbody>
              </table>
              </Col>
            </Row>
            {/* the table of data in the large media */}
            <Row className='main-content justify-content-md-center'>
              <Col className='col-md-auto'>
                <table className='no-mobile table table-bordered add3-table'>
                  <tbody>
                    <tr>
                      <th colSpan={2}>Time and cost to develop on your own</th>
                      <th></th>
                      <th colSpan={2}>Time and cost using Add3</th>
                    </tr>
                    
                    <tr>
                      <td>Number of Full Time Developers</td>
                      <th>
                        <Developer onIntegerChange={this.handleIntegerChange} />
                      </th>
                      <td></td>
                      <td>Developers needed</td>
                      <th>
                        0
                      </th>
                    </tr>
                    <tr>
                      <td>Contracts Selected</td>
                      <th>
                        <ContractSelected selectedContract={selectedContract}/>
                      </th>
                      <td></td>
                      <td>Contracts Selected</td>
                      <th>
                        <ContractSelected selectedContract={selectedContract}/>
                      </th>
                    </tr>
                    <tr>
                      <td>Days to go live with 1 Dev</td>
                      <th>
                        <DaysLiveOneReadOnly selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                      </th>
                      <td></td>
                      <td>Days to go live with 0 Dev</td>
                      <th>1</th>
                    </tr>
                    <tr>
                      <td>Days to go live with current Dev</td>
                      <th>
                        <DaysLiveCurrentDev selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} developerValue={developerValue} />
                      </th>
                      <td></td>
                      <td>Native Gas Token Fee</td>
                      <th>
                        <LPTokenFee selectedContract={selectedContract}/>
                      </th>
                    </tr>
                    <tr>
                      <td>Dev Cost Before Go-live<span className='sub-text'>2</span></td>
                      <th>
                        <DevCostLive selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                      </th>
                      <td></td>
                      <td>Dev Cost Before Go-live</td>
                      <th>
                        { devCostBeforeGolive }
                      </th>
                    </tr>
                    <tr>
                      <td>Audit Cost Before Go-live</td>
                      <th>
                        <AuditCostLive selectedContract={selectedContract} />
                      </th>
                      <td></td>
                      <td>Audit Cost Before Go-Live<span className='sub-text'>1</span></td>
                      <th>
                        <Add3AuditCostLive selectedContract={selectedContract} />
                      </th>
                    </tr>
                    <tr>
                      <td>Working Day Delay by Audit</td>
                      <th>
                        <DelayDays selectedContract={selectedContract} developerValue={developerValue} />
                      </th>
                      <td></td>
                      <td>Working Day Delay by Audit</td>
                      <th>
                        { workingDelayDayAdd3 } Days
                      </th>
                    </tr>
                    <tr>
                      <th>Total Audit and Dev Costs</th>
                      <th>
                        <TotalAuditDevCost selectedContract={selectedContract} serviceSelectOption={serviceSelectOption} />
                      </th>
                      <td></td>
                      <th>Token fee + Audit Cost</th>
                      <th>
                        <Add3TotalDevCost selectedContract={selectedContract} />
                      </th>
                    </tr>
                  </tbody>
                </table>
                <div className='bottom-text'>
                  <p>
                  1 Extra audit are optional and not mandatory, however, we highly recommend doing at least one per deployed contract.
                  </p>
                  <p>
                    2 Depending on the geographical location of your employees, salaries vary.  
                  </p> 
                </div>
              </Col>
            </Row>

            
      </Container>  
      </div>
    );
  }
}

export default App;
